#include <explainexp.h>

QString explainExp (const node * tree, const QMap<QString,operatorInfo> & operatorsMap, const QMap<QString, varInfo> & varMap, int & sentenceNumber) 
{
	QString explaining;	//�������� ���������
	QStringList operandsList;	//������ ����� � ����������
	//��� ������� ���� ������ ���������� ��������� ��� �������� ���������
	for (int i=0; i<tree->child.size();i++)
	{
		if (operatorsMap.contains(tree->child[i]->parent))
		{
			explaining += explainExp (tree->child[i], operatorsMap, varMap, sentenceNumber);
			(tree->child[i]->parent="!"+QString::number(sentenceNumber)+"!");
			sentenceNumber++;
		}
	}
	//��������� �������� � �������������� "����������� �����"
	for (int i=0;i < tree->child.size();i++)
		operandsList.append(tree->child[i]->parent);
	//������ ������� ������
	if (sentenceNumber!=1)
		explaining+="\r\n";
	explaining +=QString::number(sentenceNumber)+". "+createSentence(tree->parent, operandsList, operatorsMap, varMap);
	return explaining;
}

QString createSentence (const QString & theOperator, const QStringList & operands, const QMap<QString,operatorInfo> & operatorsMap, const QMap<QString, varInfo> & varMap) 
{
	bool isDigit;	//�������� �� ������� ������
	bool isFunction=true;	//�������� �� ������� ��������
	QString sentence;		//�����������
	//����������� ����������� ������
	sentence=operatorsMap[theOperator].pattern;
	if (sentence.contains("%1i%")||sentence.contains("%1r%")||sentence.contains("%1d%")||sentence.contains("%1v%"))
		isFunction=false;
	//���� �������
	if (isFunction)
	{
		sentence+=" (��������� �������: ";
		for (int i=0; i<operands.size();i++)
		{
			operands[i].toInt(&isDigit);
			if (isDigit)
			{
				sentence+=operands[i]+"; ";
			}
			else if (operands[i][0]=='!' && operands[i][operands[i].length()-1]=='!')
			{
				operands[i].mid(1,operands[i].length()).toInt();
				sentence+="(���������, ������������ � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')'+"; ";
			}
			else 
			{
				sentence+=varMap[operands[i]].im +"; ";
			}
		}
		sentence[sentence.length()-2]=')';
		sentence.remove(sentence.length()-1,1);
	}
	//���� ����������
	else
	{
		for (int i=0; i<operands.size();i++)
		{
			operands[i].toInt(&isDigit);
			operands[i].toFloat(&isDigit);
			operands[i].toDouble(&isDigit);
			//���� �����
			if (isDigit)
			{
				if(i==0)
				{
					if (sentence.contains("%1i%"))
						sentence.replace("%1i%",operands[i]);
					else if (sentence.contains("%1r%"))
						sentence.replace("%1r%",operands[i]);
					else if (sentence.contains("%1d%"))
						sentence.replace("%1d%",operands[i]);	
					else if (sentence.contains("%1v%"))
						sentence.replace("%1v%",operands[i]);
				}
				else
				{
					if (sentence.contains("%2i%"))
						sentence.replace("%2i%",operands[i]);
					else if (sentence.contains("%2r%"))
						sentence.replace("%2r%",operands[i]);
					else if (sentence.contains("%2d%"))
						sentence.replace("%2d%",operands[i]);	
					else if (sentence.contains("%2v%"))
						sentence.replace("%2v%",operands[i]);
				}
			}
			//���� "���������, ������������ � ����������� �..."
			else if (operands[i][0]=='!' && operands[i][operands[i].length()-1]=='!')
			{
				if(i==0)
				{
					if (sentence.contains("%1i%"))
						sentence.replace("%1i%","(���������, ������������ � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');
					else if (sentence.contains("%1r%"))
						sentence.replace("%1r%","(����������, ������������� � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');
					else if (sentence.contains("%1d%"))
						sentence.replace("%1d%","(����������, ������������� � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');
					else if (sentence.contains("%1v%"))
						sentence.replace("%1v%","(���������, ������������ � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');
				}
				else
				{
					if (sentence.contains("%2i%"))
						sentence.replace("%2i%","(���������, ������������ � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');
					else if (sentence.contains("%2r%"))
						sentence.replace("%2r%","(����������, ������������� � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');
					else if (sentence.contains("%2d%"))
						sentence.replace("%2d%","(����������, ������������� � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');	
					else if (sentence.contains("%2v%"))
						sentence.replace("%2v%","(���������, ������������ � ����������� �" + operands[i].mid(1,operands[i].length()-2)+')');
				}
			}
			//���� ����������
			else 
			{
				if(i==0)
				{
					if (sentence.contains("%1i%"))
						sentence.replace("%1i%",varMap[operands[i]].im);
					else if (sentence.contains("%1r%"))
						sentence.replace("%1r%",varMap[operands[i]].rod);
					else if (sentence.contains("%1d%"))
						sentence.replace("%1d%",varMap[operands[i]].dat);
					else if (sentence.contains("%1v%"))
						sentence.replace("%1v%",varMap[operands[i]].vin);
				}
				else
				{
					if (sentence.contains("%2i%"))
						sentence.replace("%2i%",varMap[operands[i]].im);
					else if (sentence.contains("%2r%"))
						sentence.replace("%2r%",varMap[operands[i]].rod);
					else if (sentence.contains("%2d%"))
						sentence.replace("%2d%",varMap[operands[i]].dat);	
					else if (sentence.contains("%2v%"))
						sentence.replace("%2v%",varMap[operands[i]].vin);
				}
			}
		}
	}
	sentence+=". ";
	return sentence;
}

errorInfo writeToFile (const QString & filename, const QString & explaining)
{
	errorInfo error;	//������
	error.code=0;
	QFile outputFile(filename);	//�������� ����
	if (outputFile.open(QIODevice::WriteOnly))
	{
		QTextStream stream(&outputFile);
		stream << explaining;
		outputFile.close();
		//���� ������ ���������
		if (stream.status() != QTextStream::Ok)
		{
			error.code=14;
		}
	}
	else 
		//���� ���� �� ��������
		error.code=14;
	return error;
}




