/*!
 *\file initializefunction.h
 *\brief ������������ ���� ������� ������������� ���������� � �����������.
 */
#pragma once
#include <QMap>
#include <QString>
#include <node.h>
#include <operatorInfo.h>
#include <varInfo.h>
#include <errorInfo.h>

/*!
 *\brief ������� ������������� ���������� � �����������.
 *\param[out] operatorsMap ��������� � ����������� �� ���������� � �������� ���������.
 */
void initializeOperators (QMap <QString, operatorInfo> & operatorsMap);
