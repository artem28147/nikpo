#include <QtCore/QCoreApplication>
#include <QTextCodec>
#include <explainexp.h>

int main(int argc, char *argv[])
{
	setlocale(LC_ALL, "");
	QTextCodec *codec=QTextCodec::codecForName("CP1251");
	QTextCodec::setCodecForLocale(codec);
	QTextCodec::setCodecForCStrings(codec);
	errorInfo error;		//��������� ��� ������
	error.code=0;			//���������� ��� ������ - 0
	QString explaining;		//������ ��� �������� ���������
	QStringList expression;	//���������, �������� �� �������
	QMap <QString, operatorInfo> operatorsMap;	//��������� � ����������� � ���������
	QMap <QString, varInfo> varMap;				//��������� � �����������
	int sentenceNumber=1;	//����� �� ����� ����������� ����� ������� ����������
	node* tree;				//������� ���� ������
	//�������������� ��������� � �����������
	initializeOperators(operatorsMap);			
	//���� � ��������� ������ ������ �� 2 ��������� - ����� �� ���������
	if (argc!=2)
	{
		printf("������� ���� �������� - ��� �������� �����");
		return 0;
	}
	//������ ��� �������� ����� ��� �������� ��������� ������
	QString inputFilename=argv[1];
	if (inputFilename[0]=='-')
		inputFilename.remove(0,1);
	//����������� ��� ��������� ����� � ������ ��� ���������� �� exp
	QString outputFilename=inputFilename;
	outputFilename.remove(outputFilename.length()-3,3);
	outputFilename+="exp";
	//������ ������� ������ �� �����
	error=readFromFile(inputFilename, expression, operatorsMap, varMap);
	if (error.code!=0)
	{
		if (error.code==6)
			printf("������ - �� ���� ������� ���� ��� ������");
		else if (error.code==13)
			printf("������ - � ������ ������ ����������� ���������");
		else if (error.code==3)
			printf ("������ - ����������� ������ � ����� ����������. ������: %d, �������: %d", error.string, error.pos);
		else if (error.code==8)
			printf ("������ - �������� �������� ����������. ������: %d", error.string);
		else if (error.code==10)
			printf ("������ - ����������� ����� ��� �������� ����������. ������: %d", error.string);
		else if (error.code==4)
			printf ("������ - ����������� ������ � ����� �������. ������: %d, �������: %d", error.string, error.pos);
		else if (error.code==9)
			printf ("������ - ����������� ����� ��� �������� �������. ������: %d", error.string);
		else if (error.code==7)
			printf ("������ - � �������� ������� ����������� ���������� ����������. ������: %d", error.string);	
		return 0;
	}
	//���������� ������ ���������
	tree=readToTree(expression, operatorsMap, varMap, error);
	if (error.code!=0)
	{
		if (error.code==12)
			printf ("������ - ������� � ���������� ����� ���������� ���. ������: %d, ������� %d", error.string, error.pos);
		else if (error.code==11)
			printf ("������ - ��� ������� �� ����� ���� ������. ������: %d, ������� %d", error.string, error.pos);
		else if (error.code==1)
			printf ("������ - ���������� ���������. ������: %d, ������� %d", error.string, error.pos);
		else if (error.code==5)
			printf ("������ - ���������� ��� ������� �� �������. ������: %d, ������� %d", error.string, error.pos);
		else if (error.code==2)
			printf ("������ - ����������� ���������. ������: %d, ������� %d", error.string, error.pos);	
		else if (error.code==3)
			printf ("������ - ������ ������. ������: %d, �������: %d", error.string, error.pos);
		return 0;
	}
	//���������� �������� ���������
	explaining=explainExp(tree, operatorsMap, varMap, sentenceNumber);
	//���������� ��� �������� ������� ����� ��������� ����� � ��������
	explaining.remove(explaining.length()-1,1);
	//���������� �������� � ������
	error=writeToFile(outputFilename, explaining);
	if (error.code!=0)
	{
		if (error.code==14)
			printf("������ - �� ���� ������� ���� ��� ������");
		return 0;
	}
	return 0;
}
