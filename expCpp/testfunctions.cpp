#include "testfunctions.h"

Q_DECLARE_METATYPE(node*);
Q_DECLARE_METATYPE(QStringList);
typedef QMap<QString, operatorInfo> operMap;
typedef QMap<QString, varInfo> varsMap;
Q_DECLARE_METATYPE(operMap);
Q_DECLARE_METATYPE(varsMap);
Q_DECLARE_METATYPE(errorInfo);

TestFunctions::TestFunctions(QObject *parent)
	: QObject(parent)
{

}

bool TestFunctions::compareNodes(node* left, node* right)
{
	bool result=true;

	result = result && left->parent==right->parent;
	result = result && left->child.size()==right->child.size();
	if(result)
    {
         for(int i=0;i<left->child.size();i++)
         {
             bool subresult=false;
             for(int j=0;j<left->child.size();j++)
             {
                 subresult=subresult || compareNodes(left->child[i],right->child[i]);
             }
             result=result && subresult;
         }
	}
	return result;
}

void TestFunctions::testReadToTree_data()
{
	QStringList expression;
	QMap<QString,operatorInfo> operatorsMap;
	initializeOperators(operatorsMap);
	QMap<QString, varInfo> varMap;
	errorInfo error;
	errorInfo resultError;
	
	QTest::addColumn <QStringList> ("expression");
	QTest::addColumn <QMap<QString,operatorInfo>> ("operators");
	QTest::addColumn <QMap<QString, varInfo>> ("vars");
	QTest::addColumn <errorInfo> ("error");
	QTest::addColumn <errorInfo> ("resultError");
	QTest::addColumn <node*> ("tree");
	node* firstTestTree = new node;
	operatorInfo testOper;
	varInfo testVar;
	expression<<"10";
	error.code=0;
	resultError.code=0;
	firstTestTree->parent="10";
	
	QTest::newRow ("Only one figure")<<expression<<operatorsMap<<varMap<<error<<resultError<<firstTestTree;

	node* secondTestTree = new node;
	expression.clear();
	expression.append("_va1_riable");
	varMap.insert("_va1_riable", testVar);
	secondTestTree->parent="_va1_riable";

	QTest::newRow ("Only one variable")<<expression<<operatorsMap<<varMap<<error<<resultError<<secondTestTree;
	node* thirdTestTree = new node;
	expression.clear();
	expression<<"a"<<"b"<<"10"<<"func";
	testOper.operandsCount=3;
	operatorsMap.insert("func",testOper);
	varMap.insert("a",testVar);
	varMap.insert("b",testVar);
	thirdTestTree->parent="func";
	node* thirdChildOne=new node;
	thirdChildOne->parent="10";
	node* thirdChildTwo=new node;
	thirdChildTwo->parent="b";
	node* thirdChildThree=new node;
	thirdChildThree->parent="a";
	thirdTestTree->child.append(thirdChildOne);
	thirdTestTree->child.append(thirdChildTwo);
	thirdTestTree->child.append(thirdChildThree);

	QTest::newRow ("Three arguments")<<expression<<operatorsMap<<varMap<<error<<resultError<<thirdTestTree;

	node* fourthTestTree = new node;
	expression.clear();
	expression.append("a");
	expression.append("10");
	expression.append("+");
	fourthTestTree->parent="+";
	node* fourthChildOne = new node;
	fourthChildOne->parent="10";
	node* fourthChildTwo = new node;
	fourthChildTwo->parent="a";
	fourthTestTree->child.append(fourthChildOne);
	fourthTestTree->child.append(fourthChildTwo);

	QTest::newRow ("Sum figure and var")<<expression<<operatorsMap<<varMap<<error<<resultError<<fourthTestTree;

	node* fifthTestTree = new node;
	expression.clear();
	expression<<"a"<<"b"<<"==";
	fifthTestTree->parent="==";
	node* fifthChildOne = new node;
	fifthChildOne->parent="b";
	node* fifthChildTwo = new node;
	fifthChildTwo->parent="a";
	fifthTestTree->child.append(fifthChildOne);
	fifthTestTree->child.append(fifthChildTwo);

	QTest::newRow ("Compare vars")<<expression<<operatorsMap<<varMap<<error<<resultError<<fifthTestTree;

	node* sixthTestTree = new node;
	expression.clear();
	expression<<"height"<<"width"<<"<"<<"myBool"<<"=";
	varMap.insert("height",testVar);
	varMap.insert("width",testVar);
	varMap.insert("myBool",testVar);
	sixthTestTree->parent="=";
	node* sixthChildOne = new node;
	sixthChildOne->parent="myBool";
	node* sixthChildTwo = new node;
	sixthChildTwo->parent="<";
	node* sixthChildThree = new node;
	sixthChildThree->parent="width";
	node* sixthChildFour=new node;
	sixthChildFour->parent="height";
	sixthChildTwo->child.append(sixthChildThree);
	sixthChildTwo->child.append(sixthChildFour);
	sixthTestTree->child.append(sixthChildOne);
	sixthTestTree->child.append(sixthChildTwo);

	QTest::newRow ("Logical expression")<<expression<<operatorsMap<<varMap<<error<<resultError<<sixthTestTree;

	node* seventhTestTree = new node;
	expression.clear();
	expression<<"array"<<"5"<<"getElement"<<"10"<<"power"<<"a"<<"b"<<"*"<<"+";
	varMap.insert("array",testVar);
	testOper.operandsCount=2;
	operatorsMap.insert("power",testOper);
	operatorsMap.insert("getElement",testOper);
	seventhTestTree->parent="+";

	node* seventhChildOne=new node;
	seventhChildOne->parent="*";
	node* seventhChildTwo=new node;
	seventhChildTwo->parent="power";
	node* seventhChildThree=new node;
	seventhChildThree->parent="b";
	node* seventhChildFour=new node;
	seventhChildFour->parent="a";
	node* seventhChildFive=new node;
	seventhChildFive->parent="10";
	node* seventhChildSix=new node;
	seventhChildSix->parent="getElement";
	node* seventhChildSeven=new node;
	seventhChildSeven->parent="5";
	node* seventhChildEight=new node;
	seventhChildEight->parent="array";

	seventhChildSix->child.append(seventhChildSeven);
	seventhChildSix->child.append(seventhChildEight);

	seventhChildOne->child.append(seventhChildThree);
	seventhChildOne->child.append(seventhChildFour);

	seventhChildTwo->child.append(seventhChildFive);
	seventhChildTwo->child.append(seventhChildSix);

	seventhTestTree->child.append(seventhChildOne);
	seventhTestTree->child.append(seventhChildTwo);
	
	QTest::newRow ("Sophisticated expression")<<expression<<operatorsMap<<varMap<<error<<resultError<<seventhTestTree;

	node* eighthTestTree = new node;
	expression.clear();
	expression<<"8"<<"2"<<"5"<<"*"<<"+"<<"1"<<"3"<<"2"<<"*"<<"+"<<"-"<<"/"<<"=";
	eighthTestTree->parent="error";
	error.code=1;

	QTest::newRow ("fewOperands")<<expression<<operatorsMap<<varMap<<error<<resultError<<eighthTestTree;

	node* ninethTestTree = new node;
	expression.clear();
	expression<<"a"<<"b"<<"array"<<"8"<<"2"<<"5"<<"*"<<"+"<<"/"<<"=";
	ninethTestTree->parent="error";
	error.code=2;

	QTest::newRow ("muchOperands")<<expression<<operatorsMap<<varMap<<error<<resultError<<ninethTestTree;

	node* tenthTestTree = new node;
	expression.clear();
	expression<<"a"<<"a"<<"a";
	operatorsMap.insert("a",testOper);
	tenthTestTree->parent="error";
	error.code=13;

	QTest::newRow ("varAndFuncWithEqualNames")<<expression<<operatorsMap<<varMap<<error<<resultError<<tenthTestTree;
}
void TestFunctions::testReadToTree()
{
	QFETCH(QStringList, expression);
	QFETCH(operMap, operators);
	QFETCH(varsMap, vars);
	QFETCH(errorInfo, error);
	QFETCH(errorInfo, resultError);
	QFETCH(node*, tree);
	
	node* resultTree;

	resultTree=readToTree(expression, operators,vars,resultError);

	bool result=compareNodes(tree,resultTree);

	QVERIFY2(error.code==resultError.code,"Result checking failed");
	QVERIFY2(result==true,"Result checking failed");
}

void TestFunctions::testExplainExp_data()
{
	QStringList expression;
	QString explaining;
	QMap<QString,operatorInfo> operatorsMap;
	initializeOperators(operatorsMap);
	QMap<QString, varInfo> varMap;
	errorInfo error;
	error.code=0;

	QTest::addColumn <QStringList> ("expression");
	QTest::addColumn <QMap<QString,operatorInfo>> ("operators");
	QTest::addColumn <QMap<QString, varInfo>> ("vars");
	QTest::addColumn <errorInfo> ("error");
	QTest::addColumn <QString> ("explaining");
	operatorInfo testOper;
	varInfo testVar;
	expression<<"2"<<"1"<<"+";
	explaining="������� 1 � 2.";
	QTest::newRow ("foldOneTwo")<<expression<<operatorsMap<<varMap<<error<<explaining;

	expression.clear();
	expression<<"2"<<"1"<<"+"<<"3"<<"+"<<"4"<<"+"<<"5"<<"+";
	explaining="������� 1 � 2. ������� 3 � (���������, ������������ � ����������� �1). ������� 4 � (���������, ������������ � ����������� �2). ������� 5 � (���������, ������������ � ����������� �3).";
	QTest::newRow ("foldMore")<<expression<<operatorsMap<<varMap<<error<<explaining;

	expression.clear();
	expression<<"schemesArray"<<"getFirstElement"<<"canWithstandVoltage";
	
	testVar.im="������ ����";
	testVar.rod="������� ����";
	testVar.dat="������� ����";
	testVar.vin="������ ����";
	varMap.insert("schemesArray",testVar);

	testOper.operandsCount=1;
	testOper.pattern="����� ������ ������� �������";
	operatorsMap.insert("getFirstElement", testOper);

	testOper.operandsCount=1;
	testOper.pattern="������, ��������� ������� ���������� �������� ��������� ������ �������";
	operatorsMap.insert("canWithstandVoltage", testOper);

	explaining="����� ������ ������� ������� (��������� �������: ������ ����). ������, ��������� ������� ���������� �������� ��������� ������ ������� (��������� �������: (���������, ������������ � ����������� �1)).";
	QTest::newRow ("Functions")<<expression<<operatorsMap<<varMap<<error<<explaining;

	expression.clear();
	expression<<"a"<<"b"<<"<";
	
	testVar.im="���������� a";
	testVar.rod="���������� a";
	testVar.dat="���������� a";
	testVar.vin="���������� a";
	varMap.insert("a",testVar);

	testVar.im="���������� b";
	testVar.rod="���������� b";
	testVar.dat="���������� b";
	testVar.vin="���������� b";
	varMap.insert("b",testVar);

	explaining="������ �� ���������� b, ��� ���������� a.";
	QTest::newRow ("Logical")<<expression<<operatorsMap<<varMap<<error<<explaining;

	expression.clear();
	expression<<"15"<<"arr"<<"[]";
	
	testVar.im="������ �����";
	testVar.rod="������� �����";
	testVar.dat="������� �����";
	testVar.vin="������ �����";
	varMap.insert("arr",testVar);

	explaining="����� �� ������� ����� ������� � �������� 15.";
	QTest::newRow ("Array")<<expression<<operatorsMap<<varMap<<error<<explaining;

	expression.clear();
	expression<<"25"<<"31"<<"7"<<"compareNumbers";
	
	testOper.operandsCount=3;
	testOper.pattern="�������� ��� �����";
	operatorsMap.insert("compareNumbers", testOper);

	explaining="�������� ��� ����� (��������� �������: 7; 31; 25).";
	QTest::newRow ("CompareThreeNumbers")<<expression<<operatorsMap<<varMap<<error<<explaining;

	expression.clear();
	expression<<"age"<<"student"<<".";
	
	testVar.im="�������";
	testVar.rod="��������";
	testVar.dat="��������";
	testVar.vin="�������";
	varMap.insert("age",testVar);

	testVar.im="�������";
	testVar.rod="��������";
	testVar.dat="��������";
	testVar.vin="��������";
	varMap.insert("student",testVar);

	explaining="����� ���� \"�������\" �� ��������� ��������.";
	QTest::newRow ("Struct")<<expression<<operatorsMap<<varMap<<error<<explaining;
}


void TestFunctions::testExplainExp()
{
	QFETCH(QStringList, expression);
	QFETCH(operMap, operators);
	QFETCH(varsMap, vars);
	QFETCH(errorInfo, error);
	QFETCH(QString, explaining);
	node* tree =readToTree(expression, operators,vars,error);
	int sentenceNumber=1;
	QString resultExplaining = explainExp(tree,operators,vars,sentenceNumber);
	resultExplaining.remove(resultExplaining.length()-1,1);
	QVERIFY2(resultExplaining==explaining,"Result checking failed");
}