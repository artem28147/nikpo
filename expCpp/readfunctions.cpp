#include <readfunctions.h>
errorInfo readFromFile (const QString & filename, QStringList & expression, QMap<QString,operatorInfo> & operatorsMap, QMap<QString, varInfo> & varMap)
{
	errorInfo readFileError, readFunctionsError, readVarsError;	//��������� ��� ������
	readFileError.code=0;	
	readFunctionsError.code=0;
	readVarsError.code=0;
	bool openSuccess;	//������� �� ������ ����
	QStringList input;	//������ ����� �� �������� �������
	QFile inputFile;	//������� ����

    inputFile.setFileName(filename);
    openSuccess=inputFile.open(QIODevice::ReadOnly);
	//���� ���� �� ��������, ��������� ��� ������
	if (!openSuccess)
	{
		readFileError.code=6;
		return readFileError;
	}
	//��������� ���� ���������
    while(!inputFile.atEnd()) 
		input.append(inputFile.readLine());
    inputFile.close();
	//������ ���������
	if (input.size()==0)
	{
		readFileError.code=6;
		return readFileError;
	}
	expression = readExpFromFile (input[0]);
	//������� ������ � ����������
	input.removeAt(0);

	if (expression.size()==0)
		readFileError.code=13;

	readVarsError = readVarsFromFile (input, varMap);

	readFunctionsError = readFunctionsFromFile (input, operatorsMap, varMap.size()); 

	if (readFileError.code!=0)
		return readFileError;

	else if (readFunctionsError.code!=0)
		return readFunctionsError;

	else if (readVarsError.code!=0)
		return readVarsError;

	return readFileError;
}

QStringList readExpFromFile (const QString & string)
{
	//������ ��������� � ������ ����� � ������� ��� �� �������� �����
	QStringList expression=string.split(" ",QString::KeepEmptyParts);
	for (int i=0; i<expression.size();i++)
		expression[i]=expression[i].simplified();
	return expression;
}

errorInfo readVarsFromFile (QStringList & input, QMap<QString, varInfo> & varMap)
{
	errorInfo error;	//��������� ������
	error.code=0;
	bool isVar=false;	//�������� �� ������� ����������
	QString varName;	//��� ����������
	varInfo var;		//���������� � ����������
	QStringList splittedString;
	int j;
	int i;
	if (input.size()!=0)
	{
		for (i=0; i<input.size() && !isVar; i++)
		{
			//���������� ���
			for (j=0; j<input[i].length(); j++)
			{
				if (input[i][j]==QChar('-') && input[i].mid(0,8)!=QString("function"))
				{
					isVar=true;
					break;
				}
			}
			if(isVar)
			{
				QString varName=input[i].mid(0,j);
				//������, ���� � ����� ���������� �� ������ ����� �� ����� ����� ��� ������ "_"
				if (!varName[0].isLetter() && varName[0]!=QChar('_'))
				{
					error.code=3;
					error.pos=1;
					error.string=2+i;
					return error;
				}
				//������, ���� � ����� ���������� ������������ ������� ����� "_", ���� � ����
				for (int k=1; k<varName.length(); k++)
				{
					if (!varName[k].isDigit()&&!varName[k].isLetter()&&varName[k]!=QChar('_'))
					{
						error.code=3;
						error.pos=1+k;
						error.string=2+i;
						return error;
					}
				}
				//������� �� ������ ���
				input[i].remove(0,j+1);
				//���������� �������� ����������
				splittedString=input[i].split("#");
				if(splittedString.size()!=4)
				{
					error.code=8;
					error.string=2+i;
					return error;
				}
				var.im=splittedString[0];
				var.rod=splittedString[1];
				var.dat=splittedString[2];
				var.vin=splittedString[3];
				var.vin.remove(var.vin.size()-2,2);
				//������, ���� � ��������� �������� �������� ��������� �� ��� ������
				if (var.im==QString("")||var.rod==QString("")||var.dat==QString("")||var.vin==QString("")||var.vin.data()->unicode()==13)
				{
					error.code=8;
					error.string=2+i;
					return error;
				}
				varMap.insert(varName,var);
				isVar=false;
			}
			//������, ���� ����������� ����� ��� �������� ��������
			else if(input[i].mid(0,8)!=QString("function"))
			{
				error.code=10;
				error.string=2+i;
				return error;
			}
		}
		//������� �� �������� ����� ������ � ���������� ����������
		for (int k=0; k<varMap.size(); k++)
			input.removeAt(0);
	}
	return error;
}

errorInfo readFunctionsFromFile (QStringList & input, QMap<QString,operatorInfo> & operatorsMap, int stringsAbove)
{
	errorInfo error;	//������
	error.code=0;
	QString funcName;	//��� �������
	operatorInfo func;	//���������� � �������
	QString operandsCount("nan");	//���������� ��������� �������
	int j;
	int i;
	if (input.size()!=0)
	{
		for (i=0; i<input.size(); i++)
		{
			if (input[i].mid(0,8)==QString("function"))
			{
				input[i].remove(0,9);
				j=input[i].indexOf('(');
				QString funcName=input[i].mid(0,j);
				//���� � ����� ������� ������������ ����������� �������
				if (!funcName[0].isLetter() && funcName[0]!=QChar('_'))
				{
					error.code=4;
					error.pos=10;
					error.string=2+i;
					return error;
				}
				for (int k=1; k<funcName.length(); k++)
				{
					if (!funcName[k].isDigit()&&!funcName[k].isLetter()&&funcName[k]!=QChar('_'))
					{
						error.code=4;
						error.pos=1+k+9;
						error.string=stringsAbove+i+2;
						return error;
					}
				}
				input[i].remove(0,j+1);
				j=input[i].indexOf(')');
				input[i].remove(j,1);
				if(input[i].indexOf('-')>-1)
					j=input[i].indexOf('-');
				//���������� ������
				else
				{
					error.code=9;
					error.string=stringsAbove+i+2;
					return error;
				}
				operandsCount=input[i].mid(0,j);
				if (operandsCount.toInt())
				{
					func.operandsCount=operandsCount.toInt();
				}
				//�� ������� ���������� ���������
				else
				{
					error.code=7;
					error.string=stringsAbove+i+2;
					return error;
				}
				input[i].remove(0,j+1);
				func.pattern=input[i];
				if (func.pattern.contains("\n"))
				func.pattern.remove(func.pattern.size()-2,2);
				operatorsMap.insert(funcName,func);
			}
		}
	}
	return error;
}

node* readToTree (const QStringList & expression, const QMap<QString,operatorInfo> & operatorsMap, const QMap<QString, varInfo> & varMap, errorInfo & error)
{
	QStack <node*> stack;	//����
	node* errorNode=new node;	//������������ ���� ������
	errorNode->parent="error";
	bool isDigit;	//�������� �� ������ ������
	int lastIndex=0;//������ ���������� ���������� �� ����� �������
	for (int i=0;i<expression.size();i++)
	{
		lastIndex=i;
		node* currentNode;
		currentNode=new node;
		currentNode->child.clear();
		expression[i].toInt(&isDigit);
		expression[i].toFloat(&isDigit);
		expression[i].toDouble(&isDigit);
		//��������, �� �������� �� ������� � ��������� � ����������
		if (operatorsMap.contains(expression[i])&&varMap.contains(expression[i]))
		{
			int position=0;
			for (int j=0; j<i;j++)
				position+=expression[j].length()+1;
			error.code=12;
			error.string=0;
			error.pos=position;
			return errorNode;
		}
		
		//��������, �� �������� �� ��� ������� ������
		if (expression[i]=="[]")
		{
			bool arrayIsDigit;
			expression[i-1].toInt(&arrayIsDigit);
			expression[i-1].toFloat(&arrayIsDigit);
			expression[i-1].toDouble(&arrayIsDigit);
			if (arrayIsDigit)
			{
				int position=0;
				for (int j=0; j<i;j++)
					position+=expression[j].length()+1;
				error.code=11;
				error.string=0;
				error.pos=position;
				return errorNode;
			}
		}
		//���� ��������
		if (operatorsMap.contains(expression[i]))
		{
			QMap<QString,operatorInfo>::iterator iter=operatorsMap.find(expression[i]);
			currentNode->parent=expression[i];
			if (stack.size()<iter.value().operandsCount)
			//���������� ���������
			{
				int position=0;
				for (int j=0; j<i;j++)
					position+=expression[j].length()+1;
				error.pos=position;
				error.code=1;
				error.string=0;
				
				return errorNode;
			}
			else
			{
				for(int j=0; j<iter.value().operandsCount;j++)
						currentNode->child.append(stack.pop());
			}
			stack.push(currentNode);
		}
		//���� ������� ��� �����
		else if (varMap.contains(expression[i])||isDigit)
		{
			currentNode->parent=expression[i];
			stack.push(currentNode);	
		}
		else 
		//���� ������� �� ������
		{
			if (expression[i]=="")
			{
				error.code=3;
				int position=0;
				for (int j=0; j<i;j++)
					position+=expression[j].length()+1;
				error.pos=position;
				error.string=0;
				return errorNode;
			}
			int position=0;
			for (int j=0; j<i;j++)
				position+=expression[j].length()+1;
			error.pos=position;
			error.code=5;
			error.string=0;
			return errorNode;
		}
	}
	//���� ����������� ��������� � ���������
	if(stack.size()>1)
	{
		int position=0;
		for (int j=0; j<lastIndex;j++)
			position+=expression[j].length()+1;
		error.pos=position;
		error.code=2;
		error.string=0;
		return errorNode;
	}
	return stack.pop();
}