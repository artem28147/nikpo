/*!
 *\file errorInfo.h
 *\brief ������������ ���� ��������� �������� ������.
 */
#pragma once
/*!
 *\struct errorInfo
 *\brief �������� ������.
 */
struct errorInfo
{
	int code; /**< \brief ��� ������. */
	int string; /**< \brief ����� ������ ��� ���������. */
	int pos; /**< \brief ������� ������ ��� ���������. */
};