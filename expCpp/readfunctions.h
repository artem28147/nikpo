/*!
 *\file readfunctions.h
 *\brief ������������ ���� ������� ���������� ������.
 */
#pragma once
#include <initializefunction.h>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QFile>
#include <QStack>
#include <QTextStream>
/*!
 *\brief ������� ���������� ������� ������ �� �����.
 *\param[in] filename ��� ����� �� �������� �������.
 *\param[out] expression ������ ��������� �� ������� ���������.
 *\param[in,out] operatorsMap ��������� � ����������� �� ���������� � �������� ���������.
 *\param[out] varMap ��������� � ����������� � ���������� ���������.
 *\return ���������� �� ������.
 */
errorInfo readFromFile (const QString & filename, QStringList & expression, QMap<QString,operatorInfo> & operatorsMap, QMap<QString, varInfo> & varMap);

/*!
 *\brief ������� ���������� ������� ����� ����������, �������� �� �������.
 *\param[in] string ������ � ����������.
 *\return ������ ����� � ����������, �������� �� �������.
 */
QStringList readExpFromFile (const QString & string); 


/*!
 *\brief ������� ���������� � ��������� operatorsMap ���������� � �������� ���������.
 *\param[in] input ��������� ����� �� �������� �������.
 *\param[in,out] operatorsMap ��������� � ����������� �� ���������� � �������� ���������.
 *\param[in] stringsAbove ���������� ����� �� ������� ����� �� ���������� �������
 *\return ���������� �� ������.
 */
errorInfo readFunctionsFromFile (QStringList & input, QMap<QString,operatorInfo> & operatorsMap, int stringsAbove); 

/*!
 *\brief ������� ���������� � ��������� varMap ���������� � ���������� ���������.
 *\param[in] input ��������� ����� �� �������� �������.
 *\param[in,out] varMap ��������� � ����������� � ���������� ���������.
 *\return ���������� �� ������.
 */
errorInfo readVarsFromFile (QStringList & input, QMap<QString, varInfo> & varMap); 

/*!
 *\brief ������� ���������� ������ ������� ���������.
 *\param[in] expression ������ ����� � ����������, �������� �� �������.
 *\param[in] operatorsMap ��������� � ����������� �� ���������� � �������� ���������.
 *\param[in] varMap ��������� � ����������� � ���������� ���������.
 *\param[out] error ���������� �� ������.
 *\return ��������� �� ������������ ����.
 */
node* readToTree (const QStringList & expression, const QMap<QString,operatorInfo> & operatorsMap, const QMap<QString, varInfo> & varMap, errorInfo & error); 
