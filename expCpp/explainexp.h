/*!
 *\file explainexp.h
 *\brief ������������ ���� ������� ���������.
 */
#pragma once
#include <QMap>
#include <QVector>
#include <QStack>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <readfunctions.h>
/*!
 *\brief ������� ��������� �������� ���������.
 *\param[in] tree ��������� �� ���� ������.
 *\param[in] operatorsMap ��������� � ����������� �� ���������� � �������� ���������.
 *\param[in] varMap ��������� � ����������� � ���������� ���������.
 *\param[in,out] sentenceNumber ����� ���������� ���������� ����������� (���������� ��� ������������ ������).
 *\return ������ � ��������� ���������.
 */

QString explainExp (const node * tree, const QMap<QString,operatorInfo> & operatorsMap, const QMap<QString, varInfo> & varMap, int & sentenceNumber); 

/*!
 *\brief ������� ���������� ������ �����������.
 *\param[in] theOperator ��������.
 *\param[in] operands ��������.
 *\param[in] operatorsMap ��������� � ����������� �� ���������� � �������� ���������.
 *\param[in] varMap ��������� � ����������� �� ���������.
 *\return ������������ �����������.
 */
QString createSentence (const QString & theOperator, const QStringList & operands, const QMap<QString,operatorInfo> & operatorsMap, const QMap<QString, varInfo> & varMap); 

/*!
 *\brief ������� ���������� ������� ������ �� �����.
 *\param[in] filename ��� ����� ��� ������ ��������.
 *\param[in] explaining �������� ���������.
 *\return ���������� �� ������.
 */
errorInfo writeToFile (const QString & filename, const QString & explaining);




