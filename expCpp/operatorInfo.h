/*!
 *\file operatorInfo.h
 *\brief ������������ ���� ��������� �������� ���������.
 */
#pragma once
#include <QString>
/*!
 *\struct operatorInfo
 *\brief �������� ���������.
 */
struct operatorInfo
{
	int operandsCount; /**< \brief ���������� ���������. */
	QString pattern; /**< \brief ������ ��� ��������. */
};
