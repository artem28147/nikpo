#ifndef TESTFUNCTIONS_H
#define TESTFUNCTIONS_H

#include <QObject>
#include <QTest>
#include <QMetaType>
#include <QDebug>

#include "explainexp.h"




class TestFunctions : public QObject
{
	Q_OBJECT
public:
    explicit TestFunctions(QObject *parent = 0);

private:
private slots:
	bool compareNodes(node* left, node* right);

	void testReadToTree_data();
	void testReadToTree();

	void testExplainExp_data();
	void testExplainExp();
};

#endif // TESTFUNCTIONS_H
