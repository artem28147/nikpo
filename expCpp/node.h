/*!
 *\file node.h
 *\brief ������������ ���� ��������� ���� ������.
 */
#pragma once
#include <QVector>
#include <QString>
/*!
 *\struct node 
 *\brief ���� ������.
 */
struct node
{
	QString parent; /**< \brief ������������ ����. */
	QVector<node*> child; /**< \brief �������� ����. */
};

